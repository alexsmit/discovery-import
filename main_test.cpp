#include <thread>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>

#include <boost/filesystem.hpp>

#ifdef NO_BOOST_PROCESS
#include <LinuxProcess.h>
#else
#include <boost/process.hpp>
namespace bp = boost::process;
#endif

#include "test/MainTest.h"

namespace bp = boost::process;
using namespace std;

// Note: we don't need to include RegisteredTest

int main(int argc, char **argv) {
  CppUnit::TextUi::TestRunner runner;

  string focus;
  if (argc > 1) {
    focus = argv[1];
  }

  if (focus.empty()) {
    // test from the registry
    CppUnit::TestFactoryRegistry &registry =
        CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
  }
  else {
    if (focus == "MainTest") runner.addTest(MainTest::suite());
  }


  // boost::filesystem::path full_path(boost::filesystem::current_path());
  // std::cout << "Current path is : " << full_path << std::endl;

#ifdef NO_BOOST_PROCESS
  LinuxProcess server;
  server.start("./discovery-import", {"9998", "3"});
#else
  bp::child server("./discovery-import", "9998", "3");
#endif

  std::this_thread::sleep_for(std::chrono::milliseconds(300));

  int result = (runner.run("",
                           /*wait*/ false,
                           /*printResult*/ true,
                           /*printProgress*/ true))
                   ? 0
                   : 1;

  server.terminate();

  return result;
}
