project(discovery-import)

cmake_minimum_required(VERSION 3.10)

if(CMAKE_BINARY_DIR STREQUAL CMAKE_SOURCE_DIR)
    message(FATAL_ERROR "Source and build directories cannot be the same.")
endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -O2")
set(INSTALL_DIR "/usr/local/bin")
set(USING_LIB 1)

find_package(Boost 1.65.0 COMPONENTS program_options system thread filesystem serialization REQUIRED)
find_package(PkgConfig)
pkg_search_module(CPPUNIT QUIET cppunit)

include_directories(src)

# Those files will be put to lib if necessary
file(GLOB SOURCES
    src/*.cpp
)

file(GLOB SQL_SCRIPTS
    sql/*.sql
)

file(GLOB SOURCES_TEST
    test/*.h
    test/*.cpp
)

if(USING_LIB)
    add_subdirectory(lib)
endif()

if(USING_LIB)
    add_executable(discovery-import main.cpp)
    target_link_libraries(discovery-import PRIVATE discovery-import_a)
else()
    add_executable(discovery-import main.cpp ${SOURCES})
endif()

target_link_libraries(discovery-import PRIVATE ${Boost_LIBRARIES})
target_link_libraries(discovery-import PRIVATE csv)
target_include_directories(discovery-import PRIVATE ${Boost_INCLUDE_DIR})

# Strip binary for release builds
if(CMAKE_BUILD_TYPE STREQUAL MinSizeRel)
    add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_STRIP} ${PROJECT_NAME})
endif()

# #######################
# #### test files #######
# #######################
if(NOT CMAKE_BUILD_TYPE STREQUAL MinSizeRel AND CPPUNIT_LDFLAGS)
    if(USING_LIB)
        add_executable(discovery-import-test main_test.cpp)
        target_link_libraries(discovery-import-test PRIVATE discovery-import_a discovery-import_test_a)
    else()
        add_executable(discovery-import-test main_test.cpp ${SOURCES_TEST} ${SOURCES})
    endif()

    target_link_libraries(discovery-import-test PRIVATE cppunit)
    target_link_libraries(discovery-import-test PRIVATE ${Boost_LIBRARIES})
    target_link_libraries(discovery-import-test PRIVATE csv)
    target_include_directories(discovery-import-test PRIVATE ${Boost_INCLUDE_DIR})
endif()

# #######################
# #### static files #####
# #######################
message(STATUS "*** copy configuration files to build folder ***")
file(COPY Discover-Statement-20220219.csv DESTINATION ./)

# #configure_file(file.one file.one COPYONLY)

# #######################
# #### INSTALL      #####
# #######################
if(CMAKE_BUILD_TYPE STREQUAL MinSizeRel)
    install(TARGETS discovery-import RUNTIME DESTINATION "${INSTALL_DIR}")

    # set(CMAKE_INSTALL_LIBDIR "local/lib64")
    # install(TARGETS vx-lib RUNTIME)

    # SET(CPACK_GENERATOR "RPM")
    # SET(CPACK_RPM_PACKAGE_MAINTAINER "")
    # SET(CPACK_PACKAGE_DESCRIPTION_FILE "${PROJECT_SOURCE_DIR}/discovery-import.description.txt")
    # SET(CPACK_PACKAGE_VENDOR "")
    # SET(CPACK_PACKAGE_VERSION_MAJOR 0)
    # SET(CPACK_PACKAGE_VERSION_MINOR 1)
    # SET(CPACK_PACKAGE_VERSION_PATCH 1)
    # include(CPack)
endif()
