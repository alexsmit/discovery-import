# Discovery Statement CSV export to QIF

Discovery starting from around 09/28/2022 is allowing to download CSV, XLS and PDF file for the selected statement. 

CSV file is barely usable by any financial software.
Theris also an issue with that file: it has dates in US format.

# Features

This Converter allows to
- Modify date format to YYYY-MM-DD
- Export to QIF file
- Export to QIF file maring all transaction as "cleared"

OS supported: Linux

# Usage

## Convert to QIF
discovery-import file

## Convert to QIF with cleared transactions
discovery-import file -c

## Update date format for CSV vile
discovery-import file -t csv

# How to build

## Pre-requisites

- libcsv library
- boost library (program options, filesystem)

## Build and install

./prepare_build.sh prod sudo install

After the above command an utility will be installed into /usr/local/bin
