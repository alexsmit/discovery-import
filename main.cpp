#include "stdafx.h"

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <Convert.h>

using namespace std;
using namespace boost::property_tree;
using namespace boost::archive;

using namespace boost::program_options;

int main(int argc, char **argv) {
  //+++ start of options
  options_description settings{"Options"};
  // clang-format off
  settings.add_options()
    ("help", "Help")
    ("file,f", value<string>()->implicit_value(""), "File to convert")
    ("type,t",value<string>()->default_value("qif"), "Output file type: csv or qif (default). For csv a first column will be converted to a date format usable to import by other software.")
    ("clear,c",value<string>()->implicit_value("y"), "(y/n) When exporting to QIF automatically insert 'cleared' tag (default is no)")
    ("sign,s",value<string>()->implicit_value("n"),"(y/n), Change a sign on the transaction. (default - y). You may need this if a data after an import shows expenses as income.")
    ("account,a",value<string>()->default_value(""), "Type of account and Account number to put into the QIF file (for easy matching during import).")
    ("bank,b",value<string>()->default_value(""),"Bank account type: Bank, CCard, Oth L (default), Cash")
    ;
  // clang-format on

  positional_options_description pdesc;
  pdesc.add("file", 1);

  command_line_parser parser{argc, argv};
  parser
      .options(settings)
      .positional(pdesc)
      .allow_unregistered()
      .style(
          command_line_style::default_style |
          command_line_style::allow_slash_for_short);
  parsed_options parsed_options = parser.run();

  variables_map vars;
  try {
    store(parsed_options, vars);
  }
  catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    exit(1);
  }
  notify(vars);

  if (vars.count("help") || vars.count("file") == 0) {
    cout << settings << endl;
    return 0;
  }

  //--- End of options
  Convert cvt(vars["account"].as<string>(), vars["bank"].as<string>());
  string qif;
  if (vars.count("clear")) {
    qif = boost::algorithm::to_lower_copy(vars["clear"].as<string>());
  }

  // process parameters

  string sType = vars["type"].as<string>();

  bool isQIF = qif == "y";

  bool changeSign = true;
  if (vars.count("sign")) {
    changeSign = boost::algorithm::to_lower_copy(vars["sign"].as<string>()) == "y";
  }

  // execute conversion

  cvt.Exec(vars["file"].as<string>(), sType, isQIF, changeSign);
}
