/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CSV_WRAPPER_H
#define _CSV_WRAPPER_H

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <csv.h>

#include "EntityIndexIterator.h"

using namespace std;

/// wrapper for libcsv
class CSVWrapper : iterator<forward_iterator_tag, vector<string>> {
  csv_parser parser;

public:
  bool header_done;               //!< header processed when true
  vector<string> keys;            //!< header keys
  vector<vector<string>> values;  //!< 2D values matrix
  vector<string> row;             //!< current row

  CSVWrapper();
  ~CSVWrapper();

  /// create iterator begin()
  EntityIndexIterator<CSVWrapper, vector<string>> begin();

  /// create iterator end()
  EntityIndexIterator<CSVWrapper, vector<string>> end();

  /// get row by index
  vector<string>& get(int idx);

  /// callback when line parsed
  void mcb1(void* s, size_t l);

  /// callback when everything is parsed
  void mcb2(int l);

  /// parse CSV file
  void parse(const string& filename);
};

#endif