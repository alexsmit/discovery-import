/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// Create iterator for object
template <typename Entity, typename Val>
class EntityIndexIterator {
private:
  Entity& body;
  int _idx;
  Val p;

public:
  /// create iterator for the class and set an class instance to process
  EntityIndexIterator(Entity& body, int _idx) : body(body), _idx(_idx) {}
  /// iterator implementation
  EntityIndexIterator& operator++() {
    if (_idx != -1) ++_idx;
    return *this;
  }
  /// iterator implementation
  EntityIndexIterator operator++(int) {
    EntityIndexIterator tmp(body, _idx);
    operator++();
    return tmp;
  }
  /// iterator implementation. equality operator
  bool operator==(const EntityIndexIterator& rhs) const { return _idx == rhs._idx; }
  /// iterator implementation not-equal operator
  bool operator!=(const EntityIndexIterator& rhs) const { return _idx != rhs._idx; }
  /// iterator implementation. less-then operator
  Val& operator*() { return body.get(_idx); }
};
