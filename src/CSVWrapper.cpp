/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fstream>
#include "CSVWrapper.h"

CSVWrapper::CSVWrapper() {
  header_done = false;
  csv_init(&parser, CSV_STRICT);
}

CSVWrapper::~CSVWrapper() { csv_free(&parser); }

/// create iterator begin()
EntityIndexIterator<CSVWrapper, vector<string>> CSVWrapper::begin() {
  EntityIndexIterator<CSVWrapper, vector<string>> it(*this, 0);
  return it;
}

/// create iterator end()
EntityIndexIterator<CSVWrapper, vector<string>> CSVWrapper::end() {
  EntityIndexIterator<CSVWrapper, vector<string>> it(*this, values.size());
  return it;
}

/// get row by index
vector<string>& CSVWrapper::get(int idx) {
  if (idx >= values.size()) throw runtime_error("invalid index");
  return values[idx];
}

/// callback when line parsed
void CSVWrapper::mcb1(void* s, size_t l) {
  string ss((char*)s, l);
  row.push_back(ss);
}

/// callback when everything is parsed
void CSVWrapper::mcb2(int l) {
  if (keys.size() == 0)
    keys = std::move(row);
  else {
    vector<string> r = std::move(row);
    values.push_back(r);
  }
  row.clear();
}

/// parse CSV file
void CSVWrapper::parse(const string& filename) {
  ifstream f;  //!< file stream to process, used in parse method
  values.clear();
  keys.clear();

  f.open(filename);
  if (!f.is_open()) throw runtime_error(string("Unable to open a file ") + filename);
  string sbuf;

  auto cb1 = [](void* s, size_t l, void* outfile) {
    CSVWrapper* w = static_cast<CSVWrapper*>(outfile);
    w->mcb1(s, l);
  };
  auto cb2 = [](int l, void* outfile) {
    CSVWrapper* w = static_cast<CSVWrapper*>(outfile);
    w->mcb2(l);
  };

  while (std::getline(f, sbuf)) {
    sbuf.push_back('\n');
    auto i = sbuf.length();
    // auto pcb1 = cb1;
    // auto pcb1 = std::bind(&CSVWrapper::mcb1, this, std::placeholders::_1, std::placeholders::_2,
    // std::placeholders::_3);
    if (csv_parse(&parser, sbuf.c_str(), i, cb1, cb2, this) != i) {
      throw runtime_error("Unable to parse CSV file");
    }
  }
  f.close();

  csv_fini(&parser, cb1, cb2, this);
}
