/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <iostream>
#include <fstream>
#define BOOST_SPIRIT_THREADSAFE

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include "Convert.h"
#include "CSVWrapper.h"

using namespace std;
using namespace boost::property_tree;
using namespace boost::archive;

namespace bf = boost::filesystem;

///////////////

Convert::Convert(string account, string bank_type) : account(account), bank_type(bank_type) {
  if (bank_type.empty()) bank_type = "Bank";
}

Convert::~Convert() {}

void Convert::write_line(ostream& of, vector<string> line) {
  vector<string> prep;
  std::transform(line.begin(), line.end(), std::back_inserter(prep), [](const string& v) {
    string result = v;
    if (v.find(' ') != string::npos) {
      result = string("\"") + v + "\"";
    }
    return result;
  });
  string sline = boost::algorithm::join(prep, ",");
  of << sline << endl;
}

string Convert::date2iso(const string& dt, bool exceptionIfConverted) {
  vector<string> vdt;
  boost::algorithm::split(vdt, dt, boost::algorithm::is_any_of("/"));
  if (vdt.size() != 3) {
    boost::algorithm::split(vdt, dt, boost::algorithm::is_any_of("-"));
    if (vdt.size() != 3) {
      throw runtime_error(string("wrong date in column 1: ") + dt);
    }
    else {
      if (exceptionIfConverted)
        throw runtime_error("File seems to be already converted. Date format is correct in column 1");
      return dt;
    }
  }
  string newdate = (boost::format("%s-%s-%s") % vdt[2] % vdt[0] % vdt[1]).str();
  return newdate;
}

bool Convert::ExportCSV(ostream& of, CSVWrapper& w) {
  write_line(of, w.keys);
  for (auto& r : w) {
    try {
      r[0] = date2iso(r[0]);
      r[1] = date2iso(r[1]);
    }
    catch (const std::exception& e) {
      cerr << e.what() << endl;
      return false;
    }
    write_line(of, r);
  }

  return true;
}

void Convert::ExportQIF(ostream& of, CSVWrapper& w, bool qif_cleared, bool changeSign) {
  if (!account.empty()) {
    of << "!Account" << endl;
    of << "N" << account << endl;
    of << "T" << bank_type << endl;
    of << "^" << endl;
  }

  of << "!Type:" << bank_type << endl;

  for (auto& r : w) {
    string tran = r[3];
    double dtran = boost::lexical_cast<double>(tran);
    if (changeSign) dtran = -dtran;
    tran = (boost::format("%1.2f") % dtran).str();

    of << "D" << date2iso(r[0], false) << endl;
    of << "T" << tran << endl;
    of << "P" << r[2] << endl;
    of << "M" << endl;
    if (qif_cleared) {
      of << "CX" << endl;  // transaction cleared
    }
    of << "L" << r[4] << endl;
    of << "^" << endl;
  }
}

void Convert::Exec(const string& filename, const string& type, bool qif_cleared, bool changeSigh) {
  if (type != "qif" && type != "csv") {
    cout << "invalid output format, could be only csv or qif. Entered: " << type << endl;
    ::exit(1);
  }

  cout << "Converting file : " << filename << " to type " << type << endl;
  CSVWrapper w;
  w.parse(filename);

  if (type == "csv") {
    stringstream sout;
    if (ExportCSV(sout, w)) {
      bf::path p(filename);
      p.replace_extension(".bak");
      bf::rename(filename, p);
      ofstream of(filename);
      of << sout.str();
      of.close();
    }
    return;
  }

  if (type == "qif") {
    bf::path p(filename);
    p.replace_extension(".qif");
    ofstream of(p.string());
    ExportQIF(of, w, qif_cleared, changeSigh);
    of.close();
    return;
  }
}
