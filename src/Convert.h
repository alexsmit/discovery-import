/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@replaytrader.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CONVERT_H
#define _CONVERT_H

#include <vector>
#include <string>
#include <memory>
#include <fstream>
#include "CSVWrapper.h"

using namespace boost::property_tree;
using namespace std;

class Convert {
private:
  bool ExportCSV(ostream& of, CSVWrapper& w);
  void ExportQIF(ostream& of, CSVWrapper& w, bool qif_cleared, bool changeSigh);
  void write_line(ostream& of, vector<string> line);
  string date2iso(const string& dt, bool exceptionIfConverted = true);
  
  string account, bank_type;
public:
  Convert(string account, string bank_type);
  ~Convert();

  void Exec(const string& filename, const string& type, bool qif_cleared, bool changeSigh);
};

#endif
